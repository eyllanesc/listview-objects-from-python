import QtQuick 2.14
//import QtQuick.Window 2.14
import QtQuick.Controls 2.14

ApplicationWindow {
    id: app
    visible: true
    height: 600
    width: 460

    ListView {
	width: 100
	height: 300

	model: myList
	delegate: Text {
	    text: "a = " + model.a + ", b = " + model.b
	}
    }
}
