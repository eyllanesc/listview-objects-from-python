from PySide2.QtCore import Qt, QUrl, QAbstractListModel, QModelIndex
from PySide2.QtGui import QGuiApplication

# from PySide2.QtWidgets import QApplication
from PySide2.QtQml import QQmlApplicationEngine


class MyItem:
    def __init__(self, a, b):
        self.a = a
        self.b = b


class MyList(QAbstractListModel):
    def __init__(self, parent=None):
        QAbstractListModel.__init__(self, parent)
        self.items = [MyItem(1, 2), MyItem(3, 4), MyItem(5, 6)]
        print("MyList __init__ done")

    def rowCount(self, parent=QModelIndex()):
        print("rowCount(): ", len(self.items))
        if parent.isValid():
            return 0
        return len(self.items)

    def data(self, index, role=Qt.DisplayRole):
        item = self.items[index.row()]
        name = self.roleNames().get(role, b"").decode()
        if hasattr(item, name):
            return getattr(item, name)

    def roleNames(self):
        return {Qt.UserRole + 1000: b"a", Qt.UserRole + 1001: b"b"}


if __name__ == "__main__":
    app = QGuiApplication([])
    # app = QApplication([])

    engine = QQmlApplicationEngine()
    l = MyList()
    engine.rootContext().setContextProperty("myList", l)
    engine.load(QUrl("main.qml"))

    if not engine.rootObjects():
        sys.exit(-1)

    print("app initialized")

    app.exec_()
